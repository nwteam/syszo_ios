//
//  BaseViewController.m
//  joushisu_ios
//
//  Created by 常宽 on 15/4/24.
//  Copyright (c) 2015年 SGKJ. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.view.backgroundColor =  [UIColor whiteColor];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _urlArray = [NSMutableArray array];
    // Do any additional setup after loading the view.
    UIBarButtonItem *bar = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"btn_back_off"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] landscapeImagePhone:[UIImage imageNamed:@"btn_back_on"] style:UIBarButtonItemStylePlain target:self action:@selector(comeBack)];
    self.navigationItem.leftBarButtonItem = bar;
    // 隐藏返回键
//    self.navigationItem.hidesBackButton = YES;
    
  

    
    
    
}


//广告相关
-(void)addAdImage{
    //广告条
    _adScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, self.view.frame.size.height - 50-64, self.view.frame.size.width, 50) delegate:self placeholderImage:[UIImage imageNamed:@"ad"]];
    _adScrollView.autoScrollTimeInterval = 3.0;
    _adScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    //_adScrollView.titlesGroup = titles;
    _adScrollView.currentPageDotColor = [UIColor whiteColor]; // 自定义分页控件小圆标颜色
    //_adScrollView.imageURLStringsGroup = self.tabArr;
    [self.view addSubview:_adScrollView];
}
-(void)viewDidAppear:(BOOL)animated{
     [self getAdimage];
}
-(void)getAdimage{
    NSString * url = [NSString stringWithFormat:@"%@%@",TiTleUrl,getAdImage];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    [manager POST:url parameters:@{@"type":@"1"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"结果%@", responseObject);
        NSString *result = [responseObject objectForKey:@"result"];
        if ([result isEqualToString:@"1"]) {
            NSMutableArray * imageArray = [NSMutableArray array];
            NSDictionary * tempDic = responseObject[@"data"];
            // for (int i=0; i<tempArray.count; i++) {
            [imageArray addObject:[NSString stringWithFormat:@"%@%@",AdMainUrl,tempDic[@"ad_img"]]];
            [_urlArray addObject:tempDic[@"link_url"]];
            //}
            _adScrollView.imageURLStringsGroup = imageArray;
            if (imageArray.count ==1) {
                _adScrollView.autoScroll = NO;
            }
            
        }else{
            NSLog(@"失败");
            return;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
    
    
}
#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    NSLog(@"---点击了第%ld张图片", (long)index);
    NSString * url = _urlArray[index];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}
/// 返回
- (void)comeBack
{
//    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
