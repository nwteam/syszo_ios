//
//  BaseViewController.h
//  joushisu_ios
//
//  Created by 常宽 on 15/4/24.
//  Copyright (c) 2015年 SGKJ. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <GoogleMobileAds/GoogleMobileAds.h>
#import "MBProgressHUD.h"
#import "SDCycleScrollView.h"
@import GoogleMobileAds;
@interface BaseViewController : UIViewController<SDCycleScrollViewDelegate>
{
    
    GADBannerView *bannerView_;
    SDCycleScrollView *_adScrollView;
    NSMutableArray * _urlArray;
    
}
@property (nonatomic, retain) MBProgressHUD *progress;

//广告
-(void)addAdImage;
- (void)comeBack;
@end
