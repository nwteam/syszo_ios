//
//  menuViewController.h
//  joushisu_ios
//
//  Created by 曾凯峻 on 15/4/24.
//  Copyright (c) 2015年 SGKJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface menuViewController : BaseViewController
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *tabArr;
@property (nonatomic, strong) NSMutableArray *tabArrOn;
@end
