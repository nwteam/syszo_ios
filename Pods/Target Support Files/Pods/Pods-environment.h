
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AdBrix
#define COCOAPODS_POD_AVAILABLE_AdBrix
#define COCOAPODS_VERSION_MAJOR_AdBrix 2
#define COCOAPODS_VERSION_MINOR_AdBrix 0
#define COCOAPODS_VERSION_PATCH_AdBrix 3

// AdPopcornDA
#define COCOAPODS_POD_AVAILABLE_AdPopcornDA
#define COCOAPODS_VERSION_MAJOR_AdPopcornDA 2
#define COCOAPODS_VERSION_MINOR_AdPopcornDA 0
#define COCOAPODS_VERSION_PATCH_AdPopcornDA 3

// AdPopcornOfferwall
#define COCOAPODS_POD_AVAILABLE_AdPopcornOfferwall
#define COCOAPODS_VERSION_MAJOR_AdPopcornOfferwall 2
#define COCOAPODS_VERSION_MINOR_AdPopcornOfferwall 0
#define COCOAPODS_VERSION_PATCH_AdPopcornOfferwall 5

// IgaworksCommerce
#define COCOAPODS_POD_AVAILABLE_IgaworksCommerce
#define COCOAPODS_VERSION_MAJOR_IgaworksCommerce 2
#define COCOAPODS_VERSION_MINOR_IgaworksCommerce 0
#define COCOAPODS_VERSION_PATCH_IgaworksCommerce 4

// IgaworksCore
#define COCOAPODS_POD_AVAILABLE_IgaworksCore
#define COCOAPODS_VERSION_MAJOR_IgaworksCore 2
#define COCOAPODS_VERSION_MINOR_IgaworksCore 1
#define COCOAPODS_VERSION_PATCH_IgaworksCore 1

// IgaworksCoupon
#define COCOAPODS_POD_AVAILABLE_IgaworksCoupon
#define COCOAPODS_VERSION_MAJOR_IgaworksCoupon 2
#define COCOAPODS_VERSION_MINOR_IgaworksCoupon 0
#define COCOAPODS_VERSION_PATCH_IgaworksCoupon 2

// IgaworksNanoo
#define COCOAPODS_POD_AVAILABLE_IgaworksNanoo
#define COCOAPODS_VERSION_MAJOR_IgaworksNanoo 2
#define COCOAPODS_VERSION_MINOR_IgaworksNanoo 0
#define COCOAPODS_VERSION_PATCH_IgaworksNanoo 3

// LiveOps
#define COCOAPODS_POD_AVAILABLE_LiveOps
#define COCOAPODS_VERSION_MAJOR_LiveOps 2
#define COCOAPODS_VERSION_MINOR_LiveOps 0
#define COCOAPODS_VERSION_PATCH_LiveOps 6

// SDCycleScrollView
#define COCOAPODS_POD_AVAILABLE_SDCycleScrollView
#define COCOAPODS_VERSION_MAJOR_SDCycleScrollView 1
#define COCOAPODS_VERSION_MINOR_SDCycleScrollView 62
#define COCOAPODS_VERSION_PATCH_SDCycleScrollView 0

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 7
#define COCOAPODS_VERSION_PATCH_SDWebImage 5

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 7
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 5

